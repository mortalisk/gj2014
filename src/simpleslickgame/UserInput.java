package simpleslickgame;

import org.jbox2d.callbacks.RayCastCallback;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Input;


public class UserInput
{

	private int leftCommand;
	private int rightCommand;
	private int jumpCommand;
	private int acidCommand;
	private boolean acidActive = false;
	private int boozeCommand;
	private boolean boozeActive;
	
	public UserInput()
	{
		leftCommand = Input.KEY_A;
		rightCommand = Input.KEY_D;
		jumpCommand = Input.KEY_SPACE;
		acidCommand = Input.KEY_1;
		boozeCommand = Input.KEY_2;
	}
	
	public void handleInput(Input input, GameLevel level, int delta)
	{
		if (input.isKeyPressed(Input.KEY_ESCAPE)) {
			System.exit(0);
		}
		if (input.isKeyPressed(Input.KEY_8)) {
			level.getPlayer().kill();
		}
		if (input.isKeyDown(leftCommand))
		{
			level.getPlayer().walkLeft(delta);
		} if (input.isKeyDown(rightCommand))
		{
			level.getPlayer().walkRight(delta);
		}
		if (input.isKeyPressed(jumpCommand))
		{
			level.getPlayer().jump(delta);
		}
		boolean changeTheme = false;
		if (input.isKeyPressed(acidCommand))
		{
			changeTheme = true;
			if (acidActive)
			{
				acidActive = false;
				level.activateHash(acidActive);
			}
			else
			{
				acidActive = true;

				level.activateHash(acidActive);
			}
		}
		if (input.isKeyPressed(boozeCommand))
		{
			if (!acidActive)
				changeTheme = true;
			if (boozeActive)
			{
				boozeActive = false;
				level.activateAlcohol(boozeActive);
			}
			else
			{
				boozeActive = true;
				level.activateAlcohol(boozeActive);
			}
		}
		if(changeTheme){
			if (acidActive)
				SharedTheme.Global.setNewTheme(Themes.Acid());
			else if (boozeActive)
				SharedTheme.Global.setNewTheme(Themes.Booze());
			else
				SharedTheme.Global.setNewTheme(Themes.Sober());
		}
	}
	
	
}
