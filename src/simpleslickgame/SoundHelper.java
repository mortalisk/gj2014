package simpleslickgame;

import java.io.IOException;

import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;


public class SoundHelper {
	static public Audio load(String filename)
	{
		try {
			String extension = filename.substring(filename.length()-3);
			String full_filename = "resource/sound/" + filename;
			if (extension.equals("wav")) {
				return AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream(full_filename));
			} else if (extension.equals("ogg")) {
				return AudioLoader.getStreamingAudio("OGG", ResourceLoader.getResource(full_filename));
			} else {
				throw new IOException("Loading of extension ." + extension + "\" is not implemented.");
			}
		} catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
