package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;

public class Ground implements GameObject{
	
	PhysicsBox box;
	BoxVisuals vis;
	
	public Ground(float left, float top, float right, float bottom, World w) {
		box = new PhysicsBox(new Vec2((left+right)/2.0f, (top + bottom)/2.0f), right - left, bottom -top, false, w);
		box.setTriggerObject(this);
		vis = new BoxVisuals(box);
	}

	@Override
	public void drawMe(Graphics g) {
		//box.drawMe(g);
		vis.drawMe(g);
	}

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean encloses(Vec2 p) {
		return box.encloses(p);
	}

	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JSONObject toJson() {
		JSONObject boxon = new JSONObject();
		boxon.put("name", "a ground");
		boxon.put("type", "ground");
		float left = box.getTopLeft().x;
		float top = box.getTopLeft().y;
		float right = box.getBottomRight().x;
		float bottom = box.getBottomRight().y;
		boxon.put("left", new Float(left));
		boxon.put("right", new Float(right));
		boxon.put("top", new Float(top));
		boxon.put("bottom", new Float(bottom));
		
		return boxon;
	}

	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}

	public void setTheme(VisualTheme theme) {
		if (theme.name == "booze")
			vis.addTrimTiles(true,theme.groundTrimTiles);
		else
			vis.addTrimTiles(false,theme.groundTrimTiles);
		vis.addBackgroundTiles(theme.groundBackgroundTiles);
	}
	
	public static GameObject loadFromJson(JSONObject json, World w) {
		float left = ((Double) json.get("left")).floatValue();
		float right = ((Double) json.get("right")).floatValue();
		float top = ((Double) json.get("top")).floatValue();
		float bottom = ((Double) json.get("bottom")).floatValue();
		System.out.println("Left: " + left + "right: " + right + "top: " + top + "bottom" + bottom);
		
		return new Ground(left, top, right, bottom, w);
	}

	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}
}
