package simpleslickgame;

import org.newdawn.slick.Graphics;

public interface Effect {
	boolean isDone();
	void update(int delta);
	void drawMe(Graphics g);
}
