package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Burger implements GameObject {

	float width = 50;
	float height = 50;
	private PhysicsBox physics;
	private Image image;
	private float scaleX;
	private float scaleY; 
	
	public Burger(Vec2 position, World w){
		physics = new PhysicsBox(position, width, height, true, w);
		physics.setTriggerObject(this);
	}
	
	@Override
	public JSONObject toJson() {
		JSONObject json = new JSONObject();
		json.put("name", "burger");
		json.put("type", "burger");
		json.put("x", physics.getPosition().x);
		json.put("y", physics.getPosition().y);
		return json;
	}

	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub

	}

	public static GameObject loadFromJson(JSONObject json, World w) {
		float x = ((Double) json.get("x")).floatValue();
		float y = ((Double) json.get("y")).floatValue();
		return new Burger(new Vec2(x,y),w);
	}	
	
	@Override
	public void drawMe(Graphics g) {
		Vec2 v = physics.getTopLeft();
		//g.drawRect(v.x, v.y, width, height);
		
		if(image != null)
		{ 
			image.draw(v.x,v.y,scaleX);
		}

	}

	
	@Override
	public boolean encloses(Vec2 p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTheme(VisualTheme theme) {
		try {
			image = new Image(theme.burger);
			scaleX = width/image.getWidth();
			scaleY = height/image.getHeight();
		} catch (SlickException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}

}
