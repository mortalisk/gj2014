package simpleslickgame;

import java.util.Random;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;

public class TinyboxSpam extends GameLevel {

	public TinyboxSpam(World w) {
		super(w);
	}
	
	@Override
	public void doCreateGameObjects() {
		Random gen = new Random(System.currentTimeMillis());
		
		for (int i=0; i<50; i++) {
			Box tinyBox = new Box(new Vec2(gen.nextInt(900),gen.nextInt(500)), 40f, 10f, true,w);

			this.gameObjects.add(tinyBox);
		}
		
	    float left = 0.0f;
	    float top = 0.0f;
	    float bottom = 500.0f;
	    float right = 1000.0f;
	    Box frame = new Box(left, top, bottom, right, false, w);
	    Box boxInTheMiddle = new Box(100f, 100f, 200f, 200f, false, w);
	    this.gameObjects.add(boxInTheMiddle);
	    this.gameObjects.add(frame);

	}

	@Override
	public boolean encloses(Vec2 p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}

}
