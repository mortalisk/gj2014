package simpleslickgame;

import org.jbox2d.collision.shapes.MassData;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Elevator implements GameObject {
	private float targetSpeed;
	private float ELEVATOR_WIDTH = 150.0f; 
	private float ELEVATOR_HEIGHT = 40.0f;
	final private float ELEVATOR_STREGTH = 1000000.0f;
	final private float ELEVATOR_SPEED = 10.0f;
	private Image elevatorImage;
	private PhysicsBox collisionBox;
	public Elevator(Vec2 position, World w, VisualTheme theme) {
		targetSpeed = ELEVATOR_SPEED;
		
		setTheme(theme);
		
		collisionBox = new PhysicsBox(position, ELEVATOR_WIDTH, ELEVATOR_HEIGHT, true, w);
		collisionBox.setTriggerObject(this);
		
		Body b = collisionBox.getBody();
		float defaultMass = b.getMass();
		b.setGravityScale(0.0f);
		MassData massData = new MassData();
		massData.mass = defaultMass*100.0f;
		massData.I = 1000000000.0f;
		b.setMassData(massData);
		b.setAngularDamping(10000000.0f);
		b.setAngularVelocity(0.0f);
	}
	@Override
	public JSONObject toJson() {
		JSONObject elevator = new JSONObject();
		elevator.put("name", "anElevator");
		elevator.put("type", "elevator");
		elevator.put("x", collisionBox.getPosition().x);
		elevator.put("y", collisionBox.getPosition().y);
		
		return elevator;
	}
	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}

	public static GameObject loadFromJson(JSONObject json, World w) {
		float x = ((Double) json.get("x")).floatValue();
		float y = ((Double) json.get("y")).floatValue();
		
		return new Elevator(new Vec2(x, y), w,SharedTheme.Global.getTheme());
	}

	@Override
	public void drawMe(Graphics g) {
		// TODO Auto-generated method stub
		//collisionBox.drawMe(g);
		collisionBox.setupTransform(g);
		elevatorImage.draw(-ELEVATOR_WIDTH/2.0f,-ELEVATOR_HEIGHT/2.0f,ELEVATOR_WIDTH,ELEVATOR_HEIGHT);
		g.popTransform();
	}

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		if (!(other instanceof Player)) {
			if (targetSpeed < 0.0f) {
				targetSpeed = ELEVATOR_SPEED;
			} else {
				targetSpeed = -ELEVATOR_SPEED;
			}
		}
	}

	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean encloses(Vec2 p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(int delta) {
		float currentSpeed = collisionBox.getBody().getLinearVelocity().y;
		
		float forceDirection = 1.0f;
		if (currentSpeed > targetSpeed) {
			forceDirection = -1;
		}
			
		collisionBox.getBody().applyForceToCenter(new Vec2(0.0f,forceDirection*ELEVATOR_STREGTH));
		
		// Check if the elevator reached top platform
		for (int i=-1; i<2; i+=2) {
			if (!(collisionBox.checkBlockedDirection(new Vec2(0,-5), new Vec2(i,0), ELEVATOR_WIDTH))&&
					(collisionBox.checkBlockedDirection(new Vec2(0,0), new Vec2(i,0), ELEVATOR_WIDTH))) {
				targetSpeed = ELEVATOR_SPEED;
			}
			if (collisionBox.checkBlockedDirection(new Vec2(0,0), new Vec2(0,1), ELEVATOR_HEIGHT)) {
				targetSpeed = -ELEVATOR_SPEED;
			}
		}
	
		
		
	}

	@Override
	public void setTheme(VisualTheme theme) {
		try {
			elevatorImage = new Image(theme.elevator);
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
		
	}
	
}
