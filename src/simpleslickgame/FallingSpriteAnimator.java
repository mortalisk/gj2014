package simpleslickgame;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class FallingSpriteAnimator {

	private Image falling;
	int spacing = 200;
	private ArrayList<Sprite> sprites;
	private boolean _movesOnX;
	private boolean _shouldRotate;
	private VisualTheme theme;
	
	public FallingSpriteAnimator(VisualTheme theme) {
		this.theme = theme;
		_shouldRotate = theme.fallingRotation;
		
		try {
			falling = new Image(theme.falling);
			falling.setCenterOfRotation(falling.getWidth()/3, falling.getHeight()/3);
		} catch (SlickException e) {
			e.printStackTrace();
			return;
		}
		
		Random r = new Random();
		int variation = spacing;
		sprites = new ArrayList<Sprite>();
		
		for(int i=0; i<SimpleSlickGame.screenWidth; i+=spacing){
			for(int j=0; j<SimpleSlickGame.screenHeight; j+=spacing){
				int xVar = r.nextInt(variation) - variation/2;
				int yVar = r.nextInt(variation) - variation/2;
				
				Sprite s = new Sprite(i+xVar,j+yVar);
				s.rotation = r.nextInt(360);
				s.image = falling;
				sprites.add(s);
			}
		}
	}
	
	public void update() {
		int i = 0;
		for(Sprite s : sprites){
			s.y += theme.fallingSpeedY;
			if(s.y > SimpleSlickGame.screenHeight)
				s.y = -100;
			
			if(_movesOnX){
				s.x++;
				if(s.x > SimpleSlickGame.screenWidth)
					s.x = -100;
			}
			
			if(_shouldRotate) s.rotation++;
			else s.rotation = 0;
		}		
	}

	public void draw(Graphics g) {
		int i = 0;
		for(Sprite s : sprites){
			s.draw(g);
		}
	}

}
