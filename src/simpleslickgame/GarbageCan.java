package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.Audio;

public class GarbageCan implements GameObject {
	ImageObject body;
	ImageObject lid;
	Image bodyImage;
	Image lidImage;
	Vec2 position;
	private static Audio impactSound = SoundHelper.load("trashcan.wav");
	private boolean isGnome = false;
	private boolean lidIsOn;
	public GarbageCan(Vec2 position, World w, VisualTheme theme)
	{
		try {
			bodyImage = new Image(theme.garbageCan);
			lidImage = new Image(theme.garbageCanLid);
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
		body = new ImageObject(position,bodyImage,0.2f,true,w);
		lid = new ImageObject(position,lidImage,0.2f,0.6f,true,w, 175,51);
		
		body.getPhysicsBox().setTriggerObject(this);
		lid.getPhysicsBox().setTriggerObject(this);

		this.position = position;
		
		setTheme(theme);
	}

	@Override
	public void drawMe(Graphics g) {
		// TODO Auto-generated method stub
		body.drawMe(g);
		lid.drawMe(g);
	}

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		if (isGnome) {
			if ((other instanceof Player)&&(lidIsOn)) {
				lid.getPhysicsBox().getBody().applyForceToCenter(new Vec2(0f,-50000f));
			}
		} else {
			// TODO Auto-generated method stub
			if (lidIsOn)
				impactSound.playAsSoundEffect(1.0f, 0.1f, false);
		}

		if ((thisBody == lid.getPhysicsBox().getBody()) && (otherBody == body.getPhysicsBox().getBody())) {
			lidIsOn = true;
		}
	}
	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		if ((thisBody == lid.getPhysicsBox().getBody()) && (otherBody == body.getPhysicsBox().getBody())) {
			lidIsOn = false;
		}
	}
	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean encloses(Vec2 p) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public JSONObject toJson() {
		JSONObject garbage = new JSONObject();
		garbage.put("name", "someGarbage");
		garbage.put("type", "garbage");
		garbage.put("x", new Float(this.position.x));
		garbage.put("y", this.position.y);
		
		return garbage;
	}
	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}

	public static GameObject loadFromJson(JSONObject json, World w) {
		float x = ((Double) json.get("x")).floatValue();
		float y = ((Double) json.get("y")).floatValue();
		
		return new GarbageCan(new Vec2(x, y), w, SharedTheme.Global.getTheme());
	}

	@Override
	public void setTheme(VisualTheme theme) {
		isGnome = (theme.name.equals("acid"));
		try {
			bodyImage = new Image(theme.garbageCan);
			lidImage = new Image(theme.garbageCanLid);
			body.setImage(bodyImage);
			lid.setImage(lidImage);
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
	}


}
