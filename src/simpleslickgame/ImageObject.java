package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class ImageObject implements GameObject {
	private Image spriteImage;
	private float scale;
	private PhysicsBox physics;
	private boolean showPhysicsBox;
	public PhysicsBox getPhysicsBox()
	{
		return physics;
	}
	public void setShowPhysicsBox(boolean showPhysicsBox)
	{	
		this.showPhysicsBox = showPhysicsBox; 
	}
	public ImageObject(Vec2 position, Image spriteImage, float scale, boolean dynamic, World w) 
	{
		this.spriteImage = spriteImage;
		this.scale = scale;
		this.showPhysicsBox = false;
		physics = new PhysicsBox(position, (float)spriteImage.getWidth() * scale, (float)spriteImage.getHeight() * scale, dynamic , w);
		physics.setTriggerObject(this);
	}
	public ImageObject(Vec2 position, Image spriteImage, float scale, float collision_factor, boolean dynamic, World w) 
	{
		this.spriteImage = spriteImage;
		this.scale = scale;
		this.showPhysicsBox = false;
		
		float k = scale * collision_factor;
		float width = (float)spriteImage.getWidth();
		float height = (float)spriteImage.getHeight();
		
		physics = new PhysicsBox(position, width* k,  height* k, dynamic , w);
	}
	
	public ImageObject(Vec2 position, Image spriteImage, float scale, float collision_factor, boolean dynamic, World w, float width, float height) 
	{
		this.spriteImage = spriteImage;
		this.scale = scale;
		this.showPhysicsBox = false;
		
		float k = scale * collision_factor;		
		physics = new PhysicsBox(position, width* k,  height* k, dynamic , w);
	}
	
	@Override
	public void drawMe(Graphics g) {
		if (this.showPhysicsBox) {
			physics.drawMe(g);
		}
		physics.setupTransform(g);
		g.scale(scale, scale);
		spriteImage.draw(-spriteImage.getWidth()/2, -spriteImage.getHeight()/2);
		g.popTransform();
	}
	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		
	}
	public boolean encloses(Vec2 p) {
		return physics.encloses(p);
	}
	
	public void update(int delta) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public JSONObject toJson() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}
	public void setImage(Image bodyImage) {
		this.spriteImage = bodyImage;		
	}
	@Override
	public void setTheme(VisualTheme theme) {
		
	}
	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}
}
