package simpleslickgame;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonFileReader {
	
	public JSONObject readFile(String levelName){
		FileReader f = null;
		try {
			f = new FileReader("resource/leveljson/" + levelName + ".json");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader b = new BufferedReader(f);
		String line = null;
		String json = "";
		try {
			while ((line = b.readLine()) != null) {
			    json+=line;
			    json+="\n";
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONParser parser = new JSONParser();
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj = (JSONObject) parser.parse(json);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObj;
	}

	
	private String jsonizeLevel(GameLevel level){
		String json = level.toJson().toJSONString();
		return json;
	}
	
	public void writeJson(String levelName, GameLevel level) {
		String json = jsonizeLevel(level);
		String f = "resource/leveljson/" + levelName + ".json";
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(f, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(json);
		writer.println(json);
		writer.close();
	}
}
