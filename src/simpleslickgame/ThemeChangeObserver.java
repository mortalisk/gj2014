package simpleslickgame;

public interface ThemeChangeObserver {
	public void onThemeChanged(VisualTheme theme);
}
