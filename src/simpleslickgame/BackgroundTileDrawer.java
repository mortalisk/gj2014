package simpleslickgame;

import java.awt.Rectangle;
import java.util.ArrayList;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class BackgroundTileDrawer {
	private ArrayList<Vec2> coords;
	private ArrayList<Image> images;
	private float tileWidth;
	private float tileHeight;
	private Vec2 topLeft;
	private Vec2 bottomRight;

	public BackgroundTileDrawer(Vec2 topLeft, Vec2 bottomRight, Tiles tiles) {
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
		
		coords = new ArrayList<Vec2>();	
		images = new ArrayList<Image>();
		
		tileWidth = tiles.getTileWidth();
		tileHeight = tiles.getTileHeight();
		
		for(float x=topLeft.x; x<bottomRight.x;x += tiles.getTileWidth()){
			for(float y=topLeft.y; y<bottomRight.y; y+=tiles.getTileHeight()){
				coords.add(new Vec2(x,y));
				images.add(tiles.getRandomTile());
			}
		}
	}
	
	public ArrayList<Vec2> getCoords(){
		return coords;
	}


	
	public void draw(Graphics g) {

		
		for(int i=0; i<coords.size();++i){
			Vec2 v = coords.get(i);
			
			float width = tileWidth;
			float height = tileHeight;
			
			if(v.x + tileWidth > bottomRight.x)
				width = bottomRight.x - v.x;
			
			if(v.y + tileHeight > bottomRight.y)
				height = bottomRight.y - v.y;

			images.get(i).draw(v.x,v.y, width, height);			
		}
	}
}
