package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;

public class Box implements GameObject {
	PhysicsBox collisionBox;

	public PhysicsBox getPhysicsBox()
	{
		return collisionBox;
	}
	public Box(float left, float top, float bottom, float right, boolean dynamic, World w)
	{
		collisionBox = new PhysicsBox(new Vec2((left+right)/2.0f, (top+bottom)/2.0f),right - left,bottom-top, dynamic, w);
		collisionBox.setTriggerObject(this);
	}
	public Box(Vec2 position, float width, float height, boolean dynamic, World w)
	{
		collisionBox = new PhysicsBox(position, width, height, dynamic, w);
		collisionBox.setTriggerObject(this);
	}
	@Override
	public void drawMe(Graphics g) {
		collisionBox.drawMe(g);
	}


	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean encloses(Vec2 p) {
		return collisionBox.encloses(p);
	}
	
	public void update(int delta) {
		// TODO Auto-generated method stub
	}
	@Override
	public JSONObject toJson() {
		JSONObject boxon = new JSONObject();
		boxon.put("name", "a box");
		boxon.put("type", "box");
		float left = collisionBox.getTopLeft().x;
		float top = collisionBox.getTopLeft().y;
		float right = collisionBox.getBottomRight().x;
		float bottom = collisionBox.getBottomRight().y;
		boxon.put("left", new Float(left));
		boxon.put("right", new Float(right));
		boxon.put("top", new Float(top));
		boxon.put("bottom", new Float(bottom));
		
		return boxon;
	}
	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}
	public static GameObject loadFromJson(JSONObject json, World w) {
		float left = ((Double) json.get("left")).floatValue();
		float right = ((Double) json.get("right")).floatValue();
		float top = ((Double) json.get("top")).floatValue();
		float bottom = ((Double) json.get("bottom")).floatValue();
		
		
		return new Box(left, top, bottom, right, false, w);
	}
	@Override
	public void setTheme(VisualTheme theme) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}

}
