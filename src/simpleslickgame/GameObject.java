package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;

public interface GameObject {
	public JSONObject toJson();
	public void fromJson(JSONObject json);
	public void drawMe(Graphics g);
	public void beginContact(GameObject other, Body thisBody, Body otherBody);
	public void endContact(GameObject other, Body thisBody, Body otherBody);
	public boolean encloses(Vec2 p);
	public void update(int delta);
	public void setTheme(VisualTheme theme);
}
