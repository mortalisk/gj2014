package simpleslickgame;

public class VisualTheme {

	public String falling;
	public String background;
	public boolean fallingRotation;
	public int fallingSpeedY;
	public String[] groundTrimTiles;
	public String[] groundBackgroundTiles;
	public String garbageCan;
	public String garbageCanLid;
	public String name;
	public String burger;
	public String elevator;
	
	public void setGroundTrimTiles(String ... tiles) {
		groundTrimTiles = tiles;
	}

	public void setGroundBackgroundTiles(String ... tiles) {
		groundBackgroundTiles = tiles;
	}
}
