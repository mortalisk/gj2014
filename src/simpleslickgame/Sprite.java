package simpleslickgame;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Sprite {

	public int rotation;
	public int y;
	public int x;
	public Image image;
	public float offset;

	public Sprite(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Sprite(float x, float y) {
		this.x = (int)x;
		this.y = (int)y;
	}

	public void draw(Graphics g) {
		image.setRotation(rotation);
		image.draw(x,y);
	}
}
