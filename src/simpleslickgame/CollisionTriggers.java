package simpleslickgame;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

public class CollisionTriggers implements ContactListener {
	public CollisionTriggers()
	{
	}
	@Override
	public void beginContact(Contact arg0) {
		Object ud_a = arg0.getFixtureA().getBody().getUserData();
		Object ud_b = arg0.getFixtureB().getBody().getUserData();
		GameObject go_a = (GameObject)ud_a;
		GameObject go_b = (GameObject)ud_b;
		if (go_a != null) {
			go_a.beginContact(go_b,arg0.getFixtureA().getBody(),arg0.getFixtureB().getBody());
		}
		if (go_b != null) {
			go_b.beginContact(go_a,arg0.getFixtureB().getBody(),arg0.getFixtureA().getBody());
		}
	}

	@Override
	public void endContact(Contact arg0) {
		// TODO Auto-generated method stub
		Object ud_a = arg0.getFixtureA().getBody().getUserData();
		Object ud_b = arg0.getFixtureB().getBody().getUserData();
		GameObject go_a = (GameObject)ud_a;
		GameObject go_b = (GameObject)ud_b;
		if (go_a != null) {
			go_a.endContact(go_b,arg0.getFixtureA().getBody(),arg0.getFixtureB().getBody());
		}
		if (go_b != null) {
			go_b.endContact(go_a,arg0.getFixtureB().getBody(),arg0.getFixtureA().getBody());
		}

	}

	@Override
	public void postSolve(Contact arg0, ContactImpulse arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact arg0, Manifold arg1) {
		// TODO Auto-generated method stub

	}

}
