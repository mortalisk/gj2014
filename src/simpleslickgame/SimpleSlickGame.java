package simpleslickgame;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.openal.SoundStore;

import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

public class SimpleSlickGame extends BasicGame
{
	static int screenHeight;
	static int screenWidth;
	static float scale;
	
	public Vec2 cameraPosition;

	static EffectManager effectManager;

	
	private CollisionTriggers collisionTrigger;
	static final float IMAGE_SCALE = 1f;
	World w;
	Body body;
	float time;
	private UserInput userInput = new UserInput();
	
	
    private GameLevel currentLevel;
    private int levelNr = 0;
	private Musix musix;
	public SimpleSlickGame(String gamename)
	{
		super(gamename);
	}
	
	private void victory() {
		
	}
	
	private void updateLevel(){
		levelNr++;
		w = new World(new Vec2(0, 50));

		currentLevel = new SimpleLevel(w);
		JsonFileReader p = new JsonFileReader();
		currentLevel.fromJson(p.readFile("level" + levelNr));

		collisionTrigger = new CollisionTriggers();
		w.setContactListener(collisionTrigger);	
		
		effectManager = new EffectManager();
		SharedTheme.Global.setNewTheme(Themes.Sober());

		
		musix = new Musix();
	
		
		cameraPosition = new Vec2(currentLevel.getPlayer().getPhysicsBox().getPosition().x ,
				currentLevel.getPlayer().getPhysicsBox().getPosition().y);
		
		currentLevel.start();
	}
	
	@Override
	public void init(GameContainer gc) throws SlickException {
		updateLevel();
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		w.step(0.02f, 300, 300);
		SoundStore.get().poll(0);

		userInput.handleInput(gc.getInput(), currentLevel, delta);
		//hashDrug.update(w, time);
		
		
		currentLevel.update(delta);
		effectManager.update(delta);
		
		cameraPosition.x = currentLevel.getPlayer().getPhysicsBox().getPosition().x - screenWidth/(2*scale);
		cameraPosition.y = currentLevel.getPlayer().getPhysicsBox().getPosition().y - screenHeight/(2*scale);
		if (currentLevel.getPlayer().hasWon()){
			updateLevel();
		}
		rectangle.setBounds(
				cameraPosition.x,
				cameraPosition.y, 
				screenWidth,
				screenHeight);
	}
	static public void addEffect(Effect e)
	{
		effectManager.addEffect(e);
	}
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException
	{
		
		currentLevel.drawBG(g);
		g.scale(scale, scale);
		g.translate(-cameraPosition.x, -cameraPosition.y);

		currentLevel.drawMe(g);

		effectManager.drawMe(g);


		g.resetTransform();
		currentLevel.drawHud(g);

	}

	public static void main(String[] args)
	{
		try
		{
			AppGameContainer appgc;
			appgc = new AppGameContainer(new SimpleSlickGame("Pentastic!"));
			screenHeight = appgc.getScreenHeight();
			screenWidth = appgc.getScreenWidth();
			
			scale = (float)screenHeight /800;
			
			appgc.setTargetFrameRate(60);
			appgc.setDisplayMode(screenWidth, screenHeight, true);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(SimpleSlickGame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	static Rectangle rectangle = new Rectangle(0, 0, 0, 0);
	public static Rectangle getClip() {
		return rectangle;
	}
}