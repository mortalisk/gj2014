package simpleslickgame;

import java.util.Iterator;
import java.util.LinkedList;

import org.newdawn.slick.Graphics;

public class EffectManager {
	private LinkedList<Effect> effects;
	public EffectManager()
	{
		effects = new LinkedList<Effect>();
	}
	void addEffect(Effect effect)
	{
		effects.add(effect);
	}
	void update(int delta)
	{
		Iterator<Effect> it = effects.iterator();
		while (it.hasNext()) {
			Effect effect = it.next();
			effect.update(delta);
		}
		
		
		it = effects.iterator();
		while (it.hasNext()) {
			Effect effect = it.next();
			if (effect.isDone()) {
				effects.remove(effect);
				return; // Return so that we don't get any whining about invalid iterator
			}
		}
	}
	void drawMe(Graphics g)
	{
		Iterator<Effect> it = effects.iterator();
		while (it.hasNext()) {
			Effect effect = it.next();
			effect.drawMe(g);
		}	
	}
}
