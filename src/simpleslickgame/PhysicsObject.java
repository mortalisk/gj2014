package simpleslickgame;

import org.newdawn.slick.Graphics;


public interface PhysicsObject {
	public void setupTransform(Graphics g);
	
}
