package simpleslickgame;

import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;

public class GameObjectBuilder {
	public GameObject createObjectFromJson(JSONObject json, World world){
		GameObject go = null;
		String type = (String) json.get("type");
		if (type.equals("garbage")){
			go = GarbageCan.loadFromJson(json, world);
		}else if(type.equals("box")){
			go = Box.loadFromJson(json, world);
		}else if(type.equals("elevator")){
			go = Elevator.loadFromJson(json, world);
		}else if(type.equals("needles")){
			go = Needles.loadFromJson(json, world);
		}else if(type.equals("burger")){
			go = Burger.loadFromJson(json, world);
		}else if(type.equals("ground")){
			go = Ground.loadFromJson(json, world);
		}else if(type.equals("startPos")){
			go = Player.loadFromJson(json, world);
		}else{
			throw new RuntimeException("Unkown type read from json: " + type);
		}
		go.fromJson(json);
		return go;
	}
}
