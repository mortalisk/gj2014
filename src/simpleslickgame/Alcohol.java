package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;

public class Alcohol
{

	private boolean inUse = false;
	private Vec2 originalGravity = new Vec2();
	private World w;
	private float time;
	private GameLevel gameLevel;
	
	Alcohol(World w, GameLevel gameLevel)
	{
		this.w = w;
		this.gameLevel = gameLevel;
		originalGravity = w.getGravity().clone();
		
	}
	
	public boolean isInUse() {
		return inUse;
	}
	
	public void setInUse(boolean value)
	{
		inUse = value;
		if (value)
		{
			//originalGravity = w.getGravity().clone();
			System.out.println("Gravity = x" + originalGravity.x + " y" + originalGravity.y );
			gameLevel.getPlayer().setAlcoholJumpForce();
			gameLevel.getPlayer().setUseAlcohol(true);
		}
		else
		{
			System.out.println("Original gravity");
			w.getGravity().x = originalGravity.x;
			System.out.println("Gravity = x" + originalGravity.x + " y" + originalGravity.y );
			gameLevel.getPlayer().setDefaultJumpForce();
			gameLevel.getPlayer().setUseAlcohol(false);
		}
	}
	
	
	
	public void update(float t)
	{
		 time += 0.1f;
//		if (time >= Math.PI * 2) {
//			time = 0.0f;
//		}
		
		if (inUse) {
			Vec2 gravity = w.getGravity();
			gravity.x = (float)Math.cos(time/5) * 25;
			
			w.setGravity(gravity);
		}
	}
	
	
	
}
