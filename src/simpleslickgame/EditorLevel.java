package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Graphics;

public class EditorLevel extends GameLevel {

	private GameObject active;

	public EditorLevel(World w) {
		super(w);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doCreateGameObjects() {
		

	}
	
	public void addObject(GameObject o) {
		gameObjects.add(o);
	}
	
	public void markObject(float x, float y) {
		for (GameObject o : gameObjects) {
			if (o.encloses(new Vec2(x,y)))
				active = o;
		}
	}

	@Override
	public boolean encloses(Vec2 p) {
		return false;
	}

	public void delete() {
		System.out.println("Delete");
		gameObjects.remove(active);
	}

	@Override
	public void setTheme(SharedTheme theme) {
		// TODO Auto-generated method stub
		
	}
}
