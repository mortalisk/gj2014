package simpleslickgame;

import org.jbox2d.callbacks.RayCastCallback;
import org.jbox2d.collision.shapes.ChainShape;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.Graphics;

public class PhysicsBox implements PhysicsObject {
	private float width;
	private float height;
	private Body body;
	private boolean result;
	final static float PHYSICS_SCALE = 10.0f;
	public boolean checkBlockedDirection(Vec2 localOrigin, Vec2 localVector, float triggerDistance)
	{
		result = false;
		
		Vec2 startPoint = body.getWorldPoint(localOrigin.mul(1f/PHYSICS_SCALE));
		Vec2 worldVector = body.getWorldVector(localVector);
		worldVector.normalize();
		worldVector = worldVector.mul(triggerDistance/PHYSICS_SCALE);
		Vec2 endPoint = startPoint.add(worldVector);
		
		body.getWorld().raycast(new RayCastCallback()
		{
			@Override
			public float reportFixture(Fixture arg0, Vec2 arg1, Vec2 arg2, float arg3)
			{
				if (arg0.getBody() != body) {
					if (arg0.getBody().getUserData() instanceof GameObject)
					{
						if (arg3 < 1.0f)
						{
							result = true;
						}
					}
				}
				return 0;
			}
		}, startPoint, endPoint);
		
		return result;
	}
	public float getAngle()
	{
		return body.getAngle();
	}
	public Vec2 getPosition()
	{
		return body.getPosition().mul(PHYSICS_SCALE);
	}
	public void setPosition(Vec2 position)
	{
		body.setTransform(position.mul(1.0f/PHYSICS_SCALE), 0.0f);
	}
	public void setTriggerObject(GameObject to)
	{
		body.setUserData(to);
	}

	private void createPhysicsBox(float x, float y, boolean dynamic, World w)
	{
		Shape shape;

		if (dynamic) {
			PolygonShape poly = new PolygonShape();
			
			poly.setAsBox(width/(2.0f*PHYSICS_SCALE), height/(2.0f*PHYSICS_SCALE));
			
			shape = (Shape)poly;
		} else {
			float left = - (width/2.0f) / PHYSICS_SCALE;
			float right = + (width/2.0f) / PHYSICS_SCALE;
			float top = - (height/2.0f) / PHYSICS_SCALE;
			float bottom = + (height/2.0f) / PHYSICS_SCALE;
			
			
			final int VERTEX_COUNT = 4;
			Vec2 array[] = new Vec2[VERTEX_COUNT];
			array[0] = new Vec2(left,top);
			array[1] = new Vec2(right,top);
			array[2] = new Vec2(right,bottom);
			array[3] = new Vec2(left,bottom);
			ChainShape chain = new ChainShape();
			chain.createLoop(array, VERTEX_COUNT);
			shape = (Shape)chain;
		}
		BodyDef bodyDef = new BodyDef();
		if (dynamic) {
			bodyDef.type = BodyType.DYNAMIC;
		} else {
			bodyDef.type = BodyType.STATIC;
		}
		body = w.createBody(bodyDef);
		body.createFixture(shape, 1.0f);
		body.setTransform(new Vec2(x/PHYSICS_SCALE,y/PHYSICS_SCALE), 0.0f);

	}
	public PhysicsBox(Vec2 position, float width, float height, boolean dynamic, World w)
	{
		this.width = width;
		this.height = height;
		
		createPhysicsBox(position.x, position.y, dynamic, w);
	}


	public void drawMe(Graphics g) {
		setupTransform(g);
		g.drawRect(-(width/2.0f),-(height/2.0f) , width , height );
		g.drawLine(-10, -10, +10, +10);
		g.drawLine(+10, -10, -10, +10);
		g.popTransform();
	}
	public float getWidth() {
		
		return width;
	}
	
	public float getHeight() {
		return height;
	}
	@Override
	public void setupTransform(Graphics g) {
		g.pushTransform();
		float x = getPosition().x;
		float y = getPosition().y;
		g.translate(x, y);
		g.rotate(0f, 0f, (float)(body.getAngle()/Math.PI * 180.0));	
	}
	
	boolean encloses(Vec2 p) {
		System.out.println("pressed: " + p);
		System.out.println("a box: " + body.getPosition());
		System.out.println("width: " + width);
		System.out.println("height: "+ height);
		return
				p.x > body.getPosition().x*PHYSICS_SCALE - width/2.0f &&
				p.x < body.getPosition().x*PHYSICS_SCALE + width/2.0f &&
				p.y > body.getPosition().y*PHYSICS_SCALE - height/2.0f &&
				p.y < body.getPosition().y*PHYSICS_SCALE + height/2.0f
			;
	}

	
	public Vec2 getBottomRight() {
		Vec2 center = getPosition();
        float right = center.x + width/2;
        float bottom = center.y + height/2;
        return new Vec2(right, bottom);
    }
	
	public Vec2 getTopLeft(){
		Vec2 center = getPosition();
		float left = center.x - width/2;
		float top = center.y - height/2;
		return new Vec2(left, top);
	}
	public Body getBody()
	{
		return body;
	}
	
	public Vec2 getCenterTop()
	{
		Vec2 centerPosition = body.getPosition();
		float angle = body.getAngle();
		angle += Math.PI/2;
		
		float x = (float) (Math.cos(angle) * (height/PHYSICS_SCALE-1f)/2);
		float y = (float) (-Math.sin(angle) * (height/PHYSICS_SCALE-1f)/2);
		
		Vec2 up = new Vec2(x,y);
		
		return up.add(centerPosition);
		
	}
	
	public Vec2 getABitLowerThanCenter()
	{
		Vec2 centerPosition = body.getPosition();
		float angle = body.getAngle();
		angle -= Math.PI/2;
		
		float x = (float) Math.cos(angle) ;
		float y = (float) -Math.sin(angle);
		
		Vec2 down = new Vec2(x,y);
		
		return down.add(centerPosition);
		
	}
	
	public Vec2 getCenterBottom()
	{
		Vec2 centerPosition = body.getPosition();
		float angle = body.getAngle();
		angle -= Math.PI/2;
		
		float x = (float) (Math.cos(angle) * (height/PHYSICS_SCALE-1f)/2);
		float y = (float) (-Math.sin(angle) * (height/PHYSICS_SCALE-1f)/2);
		
		Vec2 down = new Vec2(x,y);
		
		return down.add(centerPosition);
		
	}
	
	public Vec2 getFurtherDown()
	{
		Vec2 centerPosition = body.getPosition();
		float angle = body.getAngle();
		angle -= Math.PI/2;
		
		float x = (float) (Math.cos(angle) * (height/PHYSICS_SCALE+1f)/2);
		float y = (float) (-Math.sin(angle) * (height/PHYSICS_SCALE+1f)/2);
		
		Vec2 down = new Vec2(x,y);
		
		return down.add(centerPosition);
	}
	
}
