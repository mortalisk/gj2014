package simpleslickgame;

import editor.character.Animation;
import editor.character.BodyNode;
import editor.character.Character;
import editor.character.CharacterXML;

import org.jbox2d.callbacks.RayCastCallback;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.World;
import org.jbox2d.testbed.tests.BodyTypes;
import org.json.simple.JSONObject;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Player implements GameObject
{

	private Character acidCharacter;
	private Character hoboCharacter;
	private Character currentCharacter;
	private Animation currentAnimation;
	float animationFrames = 0;
	float totalAnimationFrames = 40;
	float transformToDegrees = 180.0f / (float) Math.PI;
	String animationName = "walk";
	PhysicsBox collisionBox;
	World world;
	
	// magic numbers bellow
	Vec2 defaultJumpForce = new Vec2(0.0f, -80000.0f);
	Vec2 alcoholJumpForce = new Vec2(0.0f, -180000.0f);
	Vec2 jumpForce = defaultJumpForce;
	
	Vec2 headForce;
	private boolean onGround = false;
	float targetSpeed;
	private boolean facingLeft;
	private boolean inAir;
	private int objectInContact;
	private Body sensorBody;
	private int noContact;
	private boolean victory = false;

	private boolean dead = false;
	
	private float headForceThreshold = 0.2f;
	private boolean isUsingAlcohol;
	private boolean isUsingAcid;

	private static final float WALK_STRENGTH = 200000.0f; // the force used to adjust speed to match target speed.
	private static final float WALK_SPEED = 50.0f; // the target walk speed when told to move
	public void resurrect()
	{
		dead = false;
	}
	public boolean isDead()
	{
		return dead;
	}
	public void kill()
	{
		dead = true;
	}
	public Player(World w, Vec2 start){
		collisionBox = new PhysicsBox(start,50,120,true,w);
		String openPath;
		String name = "hobo";
		openPath = System.getProperty("user.dir");
		openPath += "/data/characters/"+name;
	    hoboCharacter = CharacterXML.open(openPath, name, new SlickImageLoader());
	    this.setCharacter(hoboCharacter);
	    this.selectAnimation("default");
	    
	    name = "hoboOnDrugs";
		openPath = System.getProperty("user.dir");
		openPath += "/data/characters/"+name;
	    acidCharacter = CharacterXML.open(openPath, name, new SlickImageLoader());
	    
		
	    collisionBox.setTriggerObject(this);
	    collisionBox.getBody().setAngularDamping(4f);
		world = w;
		facingLeft = false;
		
		createSensor();
	}
	
	private void createSensor() {
		CircleShape sensorshape = new CircleShape();
		sensorshape.setRadius(10.0f);
	}

	public PhysicsBox getPhysicsBox(){
		return collisionBox;
	}
	
	public void setCharacter(Character character){
		System.out.println("set" + character.getName());
		
		this.currentCharacter = character;
	}

	private void playAnimation()
	{
		if (isMoving() || airborne()) {
			animationFrames++;
			if (animationFrames > totalAnimationFrames) {
				animationFrames = 0f;
			}
		}
	}
	private void adjustSpeed(int delta)
	{
		float df = (float)delta/1000.0f;
		Body body = collisionBox.getBody();
		float currentSpeed = getHorizontalSpeed();
		
		if (currentSpeed < targetSpeed) {
			body.applyForce(new Vec2(+df * WALK_STRENGTH, 0.0f),getPhysicsBox().getABitLowerThanCenter());
		} else {
			body.applyForce(new Vec2(-df * WALK_STRENGTH, 0.0f),getPhysicsBox().getABitLowerThanCenter());
		}
	}
	public float getHorizontalSpeed()
	{
		return collisionBox.getBody().getLinearVelocity().x;
	}
	public boolean isMoving()
	{
		return (Math.abs(getHorizontalSpeed()) > WALK_SPEED*0.2f);
	}
	public void drawMe(Graphics g) {
		//collisionBox.drawMe(g);
		
		g.pushTransform();
		g.setColor(Color.red);
		// g.draw(new Circle(position.x, position.y, radius));
		// g.drawString(name, position.x - (radius / 10), position.y
		// - (radius / 10));
		g.setColor(Color.white);

		if (currentCharacter != null) {
			Animation animation = currentAnimation;
			int numberOfFrames = animation.getChildCount();
			float ratio = animationFrames / totalAnimationFrames;

			int frame = (int)Math.floor(numberOfFrames * ratio);
			if(frame > numberOfFrames-1) {
				frame = numberOfFrames -1;
			}
			//g.draw(new Circle(attackPoint.x, attackPoint.y, attackRadius));
			BodyNode root = (editor.character.BodyNode) animation.getChild(frame).getChild(0);

			collisionBox.setupTransform(g);
			//g.rotate(0.0f, 0.0f, (angle + (float) Math.PI / 2) * transformToDegrees);
			if (facingLeft) {
				g.scale(-1, 1);
			}
			
			g.scale(0.5f, 0.5f);
			drawNode(root, g);
			g.scale(2.0f, 2.0f);
			
			playAnimation();

		}
		g.popTransform();
	}

	public void selectAnimation(String name) {
		if (name.equals(animationName))
			return;
		animationName = name;
		
		
		for (int i = 0; i<currentCharacter.getChildCount(); ++i) {
			Animation animation = (Animation) currentCharacter.getChild(i);
			if (animation.getName().equals(name)) {
				currentAnimation = animation;
			}
		}
	}
	
	private void drawNode(BodyNode bodyNode, Graphics g) {

		g.rotate(0.0f, 0.0f, bodyNode.getAngle() * transformToDegrees);
		g.translate(bodyNode.getDistance(), 0);

		
		for (int i = 0; i < bodyNode.getChildCount(); i++) {
			if (((BodyNode)bodyNode.getChild(i)).getzAxis() < bodyNode.getzAxis())
			{
				drawNode((BodyNode) bodyNode.getChild(i), g);
			}
		}
		
		Image image = (Image)bodyNode.getImage();
		if (image != null) {
			g.rotate(0, 0, bodyNode.getImageAngle() * transformToDegrees);
			g.drawImage(image, (-1) * image.getWidth() / 2,
					(-1) * image.getHeight() / 2, null);
			g.rotate(0, 0, -bodyNode.getImageAngle() * transformToDegrees);
		}

		for (int i = 0; i < bodyNode.getChildCount(); i++) {
			if (((BodyNode)bodyNode.getChild(i)).getzAxis() >= bodyNode.getzAxis())
			{
				drawNode((BodyNode) bodyNode.getChild(i), g);
			}
		}
		g.translate(-bodyNode.getDistance(), 0);
		g.rotate(0, 0, -bodyNode.getAngle() * transformToDegrees);

	}

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		if (other instanceof Burger) {
			victory = true;
		}
		objectInContact++;
	}
	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		objectInContact--;
	}
	@Override
	public boolean encloses(Vec2 p) {
		return false;
	}
	
	public void update(int delta) {
		float y_vector = (float)Math.cos(collisionBox.getAngle());
		if (!isUpright(0.6f)) {
			selectAnimation("worm");
		} else {
			selectAnimation("walk");
		}
		updateOnGround();
		updateAirborne();
		
		
		if (!onGround && isUpright(0.6f))
		{
			selectAnimation("air");
		}
		adjustSpeed(delta);
		
		if (isMoving() && isUpright()) {
			facingLeft = (getHorizontalSpeed() < 0.0f);
			if (!(world.getGravity().y > 0)) {
				facingLeft = !facingLeft;
			}
		}
		// Setting target speed to 0 so that if the button is not pressed the next user input
		// the hobo will aim towards speed 0.
		targetSpeed = 0.0f;
		
		applyHeadForce();
		
	}

	@Override
	public JSONObject toJson() {
		JSONObject hoboson = new JSONObject();
		hoboson.put("name", "hobo");
		hoboson.put("type", "startPos");
		hoboson.put("x", new Float(this.getPhysicsBox().getPosition().x));
		hoboson.put("y", new Float(this.getPhysicsBox().getPosition().y));
		
		return hoboson;
	}

	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}

	public static GameObject loadFromJson(JSONObject json, World w) {
		float x = ((Double) json.get("x")).floatValue();
		float y = ((Double) json.get("y")).floatValue();
		
		return new Player(w, new Vec2(x, y));
	}
	
	public void applyHeadForce()
	{
		if ( 
				isUpright(headForceThreshold ) 
				|| 
				airborne() 
				)
		{
			//collisionBox.getBody().applyTorque(-collisionBox.getAngle()*10000);
			Vec2 gravity = world.getGravity();
			Vec2 headForce = gravity.mul(5f);
			collisionBox.getBody().applyLinearImpulse(headForce, collisionBox.getCenterTop());
			Vec2 feetForce = gravity.mul(-5f);
			collisionBox.getBody().applyLinearImpulse(feetForce, collisionBox.getCenterBottom());
		}
		else if (!isUsingAcid && !isUsingAlcohol)
		{
			//collisionBox.getBody().applyTorque(-collisionBox.getAngle()*10000);
			Vec2 gravity = world.getGravity();
			Vec2 headForce = gravity.mul(15f);
			collisionBox.getBody().applyLinearImpulse(headForce, collisionBox.getCenterTop());
			Vec2 feetForce = gravity.mul(-5f);
			collisionBox.getBody().applyLinearImpulse(feetForce, collisionBox.getCenterBottom());
		}
	}
	
	void updateAirborne() {
		
		inAir = objectInContact == 0 && noContact >= 15;
		
		if (objectInContact == 0) { noContact++; }
		else { noContact = 0; }
		
//		Vec2 dir = world.getGravity().clone();
//		dir.normalize();
//		dir = dir.mul(collisionBox.getHeight()/10 +1);
//		world.raycast(new RayCastCallback()
//		{
//			@Override
//			public float reportFixture(Fixture arg0, Vec2 arg1, Vec2 arg2, float arg3)
//			{
//				if ((arg0.getBody().getUserData() instanceof Ground))
//				{
//					
//						inAir = true;
//					
//				}
//				return 0;
//			}
//		}, getPhysicsBox().getBody().getPosition(), getPhysicsBox().getBody().getPosition().add(dir));
//		
	}
	
	private boolean airborne() {
		// TODO Auto-generated method stub
		return inAir;
	}

	public boolean isUpright() {
		return isUpright(0.4f);
	}
	
	public boolean isUpright(float t)
	{
		//boolean downGravity = world.getGravity().y > 0;
		//System.out.println(collisionBox.getAngle());
		
		
		Vec2 upVector = world.getGravity().clone();
		upVector.normalize();
		upVector.mulLocal(-1);
		
		upVector.x = 0;
		
		Vec2 boxVector = collisionBox.getBody().getWorldVector(new Vec2(0, -1));
				
//				new Vec2(
//				-(float)Math.sin(collisionBox.getAngle()),
//				(float)Math.cos(collisionBox.getAngle())
//				);
		
		
		float dot = Vec2.dot(upVector, boxVector);
		float angle = (float) Math.acos(dot);
		
		
//		double upAng = Math.atan2(upVector.y, upVector.x);
//		double boxAng = Math.atan2(boxVector.y, boxVector.x);
//		
//		double and = 
		
		
		//double abs = Math.abs(Math.sin(collisionBox.getAngle()));
		if (angle <= t)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public void walkLeft(int delta)
	{
		targetSpeed = -WALK_SPEED;
	}
	public void walkRight(int delta)
	{
		targetSpeed = +WALK_SPEED;
	}
	public void stop()
	{
		targetSpeed = 0.0f;
	}
	public void jump(int delta)
	{
		if (onGround)
		{
			collisionBox.getBody().applyForceToCenter(jumpForce);
		}
	}
	
	public void updateOnGround()
	{
		onGround = false;
		world.raycast(new RayCastCallback()
		{
			@Override
			public float reportFixture(Fixture arg0, Vec2 arg1, Vec2 arg2, float arg3)
			{
				if (arg0.getBody().getUserData() instanceof GameObject)
				{
					if (arg3 < 100.0f)
					{
						onGround = true;
					}
				}
				return 0;
			}
		}, getPhysicsBox().getBody().getPosition(), getPhysicsBox().getFurtherDown());
	}

	@Override
	public void setTheme(VisualTheme theme) {
		// TODO Auto-generated method stub
		if(theme.name.equals(Themes.Acid().name))
		{
			currentCharacter = acidCharacter;
			selectAnimation("");
			selectAnimation(currentAnimation.getName());
		}
		else
		{
			currentCharacter = hoboCharacter;
			selectAnimation("");
			selectAnimation(currentAnimation.getName());
		}
	}
	
	public boolean hasWon(){
		return victory ;
	}

	public float getHeadForceThreshold()
	{
		return headForceThreshold;
	}

	

	public void setDefaultJumpForce()
	{
		jumpForce = defaultJumpForce;
	}
	
	public void setAlcoholJumpForce()
	{
		jumpForce = alcoholJumpForce;
	}
	
	public void setUseAlcohol(boolean isUsingAlcohol)
	{
		this.isUsingAlcohol = isUsingAlcohol;
	}
	public void setUseAcid(boolean isUsingAcid)
	{
		this.isUsingAcid = isUsingAcid;
	}

}
