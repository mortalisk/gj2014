package simpleslickgame;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Tiles {
	private ArrayList<Image> tiles;
	private Random rand;

	public Tiles(String ... files) throws SlickException{
		tiles = new ArrayList<Image>();
		for(String f : files){
			tiles.add(new Image(f));
		}
		
		rand = new Random();
	}
	
	public Image getTile(int index)
	{
		return tiles.get(index);
	}

	public float getTileWidth() {
		return tiles.get(0).getWidth();
	}
	
	public float getTileHeight() {
		return tiles.get(0).getHeight();
	}

	public Image getRandomTile() {
		return tiles.get(rand.nextInt(tiles.size()));
	}
	
	
	
}
