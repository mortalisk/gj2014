package simpleslickgame;

import java.util.ArrayList;
import java.util.List;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
public abstract class GameLevel implements GameObject, ThemeChangeObserver {
	protected List<GameObject> gameObjects;
	protected World w;
	private LevelBackground background;
	HashDrug hashDrug;
	Alcohol alcohol;
	protected Player player;
	private float time;
private Vec2 initialPlayerPosition;
	private Image hashImage;
	private Image alcoImage;	

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {};
	public void endContact(GameObject other, Body thisBody, Body otherBody) {};
	public GameLevel(World w){
		this.w = w;
		hashDrug = new HashDrug(w, this);
		alcohol = new Alcohol(w, this);
		
		try {
			hashImage = new Image("resource/images/drug_icons/acid.png");
			alcoImage = new Image("resource/images/drug_icons/booze.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.gameObjects = new ArrayList<GameObject>();
	}
	
	public abstract void doCreateGameObjects();
	public void setBackground(LevelBackground background){
		this.background = background;
	}
	
	private boolean verifyLevel(){
		return true;
	}
	public void start()
	{
		
	}
	public List<GameObject> createLevel() {
		 doCreateGameObjects();
		 verifyLevel();
		 return gameObjects;
	}

	public void drawBG(Graphics g){
		if(background != null)
			background.draw(g);	
	}
	
	public void drawHud(Graphics g){
		drawInventory(g);
		drawRadar(g);
	}
	private void drawInventory(Graphics g) {
		g.pushTransform();
		g.translate(20, 20);
		
		if (hashDrug.isInUse()) {
			g.setColor(Color.red);
		} else {
			g.setColor(Color.white);
		}
		
		g.drawRect(0,0, 50, 50);
		g.drawImage(hashImage, 0, 0, 50, 50, 0, 0, hashImage.getWidth(), hashImage.getHeight());
		
		g.translate(70, 0);
		
		if (alcohol.isInUse()) {
			g.setColor(Color.red);
		} else {
			g.setColor(Color.white);
		}
		g.drawRect(0,0, 50, 50);
		g.drawImage(alcoImage, 0, 0, 50, 50, 0, 0, alcoImage.getWidth(), alcoImage.getHeight());
		
		g.setColor(Color.white);
		
		g.popTransform();
		
		
	}
	private void drawRadar(Graphics g) {
		g.setColor(new Color(0,	1, 0, 0.3f));
		g.fillRect(SimpleSlickGame.screenWidth -120, 20, 100, 100);
		g.translate(SimpleSlickGame.screenWidth -70, 70);
		Vec2 grav = w.getGravity();
		g.setColor(Color.red);
		g.drawRoundRect(grav.x, grav.y, 4, 4, 1);
		g.setColor(Color.white);
	}
	
	@Override
	public void drawMe(Graphics g) {

		
		for (GameObject go : gameObjects) {
			go.drawMe(g);
		}
	}
	@Override
	public void update(int delta) {
		if (player.isDead()) {
			player.getPhysicsBox().setPosition(initialPlayerPosition);
			player.resurrect();
		}
		
		if(background != null)
			background.update();
		
		for (GameObject go : gameObjects) {
			go.update(delta);
		}
		
		hashDrug.update(0.0f);
		alcohol.update(0.0f);
	}
	public World getW()
	{
		return w;
	}
	public void setW(World w)
	{
		this.w = w;
	}
	public Player getPlayer()
	{
		return player;
	}
	public void setPlayer(Player player)
	{
		this.player = player;
	}
	
	@Override
	public JSONObject toJson() {
		System.out.println("Ready for jsoning");
		JSONObject level = new JSONObject();
		level.put("title", "jsonLevel");
		JSONArray gos = new JSONArray();
		for(GameObject go : this.gameObjects) {
			gos.add(go.toJson());
		}
		level.put("gameobjects", gos);
		JSONObject root = new JSONObject();
		root.put("level", level);
		return root;
	}
	
	public void setTheme(SharedTheme theme){
		theme.addThemeChangedObserver(this);
		onThemeChanged(theme.getTheme());
	}
	
	@Override
	public void setTheme(VisualTheme theme){
	}
	
	@Override
	public void onThemeChanged(VisualTheme theme){
		for(GameObject g : gameObjects){
			g.setTheme(theme);
		}
		if(background != null) background.setTheme(theme);
	}
	
	@Override
	public void fromJson(JSONObject root) {
		JSONObject level = new JSONObject();
		level = (JSONObject) root.get("level");
		System.out.println(level);
		
		String title = (String) level.get("title");
		System.out.println("Parsing level: " + title);
		JSONArray gosons = (JSONArray) level.get("gameobjects");
		this.gameObjects = new ArrayList<GameObject>();
		GameObjectBuilder builder = new GameObjectBuilder();
		for(Object goson : gosons) {
			GameObject go = builder.createObjectFromJson((JSONObject) goson, w);
			if (go instanceof Player) {
				this.setPlayer((Player) go);
				initialPlayerPosition = ((Player)go).getPhysicsBox().getPosition();
			}
			this.gameObjects.add(go);
		}
		this.setTheme(SharedTheme.Global);
		
	    LevelBackground bg = new LevelBackground(player.getPhysicsBox());
	    bg.setTheme(SharedTheme.Global.getTheme());
	    setBackground(bg);	
	}
	public void activateHash(boolean hashActive)
	{
		hashDrug.setInUse(hashActive);
	}
	public void activateAlcohol(boolean active)
	{
		alcohol.setInUse(active);
	}
}
