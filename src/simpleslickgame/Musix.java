package simpleslickgame;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;

public class Musix implements ThemeChangeObserver {
	private Music sober;
	private Music acid;
	private Music booze;
	private VisualTheme theme;
	private Music last;
	private float boosePos;
	private float acidPos;
	private float drunkPos;

	public Musix(){
		System.out.println("Music");
		try {
			booze = new Music("resource/music/v1/drunk_song.wav");
			sober = new Music("resource/music/v1/sober_song.wav");
			acid =  new Music("resource/music/v1/acid_song.wav");
		} catch (SlickException e) {
			e.printStackTrace();
		}
		
		boosePos = 0f;
		acidPos = 0f;
		drunkPos = 0f;
		
		theme = SharedTheme.Global.getTheme();
		SharedTheme.Global.addThemeChangedObserver(this);
		setLoop(sober, 0);	
	}
	
	private void setLoop(Music music, float pos){
		music.loop(1,100);
	}
	
	@Override
	public void onThemeChanged(VisualTheme theme) {		
		System.out.println(theme.name);
		if(theme.name == "sober"){
			setLoop(sober,0);
		}
		else if(theme.name == "acid"){
			setLoop(acid, 0);
		}
		else if(theme.name == "booze"){
			setLoop(booze, 0);
		}
	}
}
