package simpleslickgame;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import editor.character.ImageLoader;

public class SlickImageLoader implements ImageLoader
{

	@Override
	public Object loadImage(String openPath, String imageName)
	{
		try 
		{
			return new Image(openPath+"/"+imageName);
		} catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}

}
