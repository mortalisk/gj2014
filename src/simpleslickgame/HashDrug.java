package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;

public class HashDrug
{

	private boolean inUse = false;
	private Vec2 originalGravity = new Vec2();
	private World w;
	private float time;
	private GameLevel gameLevel;
	
	HashDrug(World w, GameLevel gameLevel)
	{
		this.w = w;
		this.gameLevel = gameLevel;
		originalGravity = w.getGravity().clone();
		
	}
	
	public boolean isInUse() {
		return inUse;
	}
	
	public void setInUse(boolean value)
	{
		inUse = value;
		if (value)
		{
			System.out.println("Gravity = x" + originalGravity.x + " y" + originalGravity.y );
			gameLevel.getPlayer().setUseAcid(true);
		}
		else
		{
			System.out.println("Original gravity");
			w.getGravity().y = originalGravity.y;
			System.out.println("Gravity = x" + originalGravity.x + " y" + originalGravity.y );
			gameLevel.getPlayer().setUseAcid(false);
		}
	}
	
	
	
	public void update(float t)
	{
		 time += 0.1f;
//		if (time >= Math.PI * 2) {
//			time = 0.0f;
//		}
		
		if (inUse) {
			Vec2 gravity = w.getGravity();
			gravity.y = (float)Math.sin(time/5) * Math.abs(originalGravity.y);
			
			w.setGravity(gravity);
		}
	}
	
	
	
}
