package simpleslickgame;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Particle {
	public Vec2 position;
	public Vec2 velocity;
	public Vec2 size;
	public float alpha;
	public Image image;
	public float nativeSpeed;
	public Particle()
	{	
		
	}
	Particle(Vec2 position, Vec2 velocity, Vec2 size, float alpha, Image image)
	{
		
		this.position = position;
		this.velocity = velocity;
		this.size = size;
		this.image = image;
		this.alpha = alpha;
	}
	void update(int delta)
	{
		this.position = this.position.add(velocity.mul((float)delta/1000.0f));
	}
	void draw(Graphics g)
	{
		image.setAlpha(alpha);
		g.pushTransform();
		g.translate(position.x,position.y);
		image.draw(-(size.x/2.0f),-(size.y/2.0f), size.x, size.y);
		g.popTransform();
	}

}
