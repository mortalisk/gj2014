package simpleslickgame;
import org.jbox2d.collision.shapes.MassData;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.json.simple.JSONObject;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;


public class Needles implements GameObject {
	private Box object;
	private static Image needleImage = null;
	private static final float WIDTH = 200;
	private static final float HEIGHT = 50;
	public Needles(Vec2 Position, World w)
	{
		if (needleImage == null) {
			try {
				needleImage = new Image("resource/images/pointy_things/pointy_things.png");
			} catch (SlickException e){
				e.printStackTrace();
			}
		}
		object = new Box(Position,WIDTH,HEIGHT,true,w);
		object.getPhysicsBox().setTriggerObject(this);
		object.getPhysicsBox().getBody().setGravityScale(0.0f);
		
		
		MassData massdata = new MassData();
		massdata.mass = 100000000.0f;
		massdata.I = 100000000.0f;
		object.getPhysicsBox().getBody().setMassData(massdata);
	}
	@Override
	public JSONObject toJson() {
		JSONObject elevator = new JSONObject();
		elevator.put("name", "someneedles");
		elevator.put("type", "needles");
		elevator.put("x", object.getPhysicsBox().getPosition().x);
		elevator.put("y", object.getPhysicsBox().getPosition().y);
		
		return elevator;
	}
	@Override
	public void fromJson(JSONObject json) {
		// TODO Auto-generated method stub
		
	}

	public static GameObject loadFromJson(JSONObject json, World w) {
		float x = ((Double) json.get("x")).floatValue();
		float y = ((Double) json.get("y")).floatValue();
		
		return new Needles(new Vec2(x, y), w);
	}

	@Override
	public void drawMe(Graphics g) {
		object.getPhysicsBox().setupTransform(g);
		needleImage.draw(-WIDTH/2f, -HEIGHT/2f, WIDTH, HEIGHT);
		g.popTransform();
		//object.drawMe(g);
	}

	@Override
	public void beginContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		if (other instanceof Player) {
			((Player)other).kill();
		}
	}

	@Override
	public void endContact(GameObject other, Body thisBody, Body otherBody) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean encloses(Vec2 p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setTheme(VisualTheme theme) {
		// TODO Auto-generated method stub
		
	}
	
}
