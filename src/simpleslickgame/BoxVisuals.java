package simpleslickgame;

import leveleditor.EditorGame;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class BoxVisuals{

	private PhysicsBox box;
	private float width;
	private float height;
	private Vec2 topleft;
	private Vec2 bottomright;

	public BoxVisuals(PhysicsBox boxLeft) {
		box = boxLeft;
		width = box.getWidth();
		height = box.getHeight();
		topleft = box.getTopLeft();
		bottomright = box.getBottomRight();
	}
	
	private BackgroundTileDrawer tiledCoordinates;
	private TrimTileDrawer trim;	
	
	public void addTrimTiles(boolean even, String ... files) {
		try {
			trim = new TrimTileDrawer(box.getTopLeft(), box.getBottomRight(), new Tiles(files), even);			
		} catch (SlickException e) {
			e.printStackTrace();
		}		
	}
	
	public void addBackgroundTiles(String ... files){
		try {
			tiledCoordinates = new BackgroundTileDrawer(box.getTopLeft(), box.getBottomRight(), new Tiles(files));			
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}	
	
	private boolean within(org.newdawn.slick.geom.Rectangle r, float x, float y) {
		if (EditorGame.isInEditor)
			return true;
		return x > r.getMinX() &&
				x < r.getMaxX() &&
				y > r.getMinY() &&
				y < r.getMaxY();
	}

	public void drawMe(Graphics g) {
		
		org.newdawn.slick.geom.Rectangle r = SimpleSlickGame.getClip();
		
		boolean with_in =
				within(r, topleft.x, topleft.y) ||
				within(r, bottomright.x, topleft.y) ||
				within(r, topleft.x, bottomright.y) ||
				within(r, bottomright.x, bottomright.y)
				;
		
		if (!with_in) return;
		
		if(tiledCoordinates != null)
			tiledCoordinates.draw(g);		
		
		if(trim !=null)
			trim.draw(g);
	}


}
