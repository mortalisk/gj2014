package simpleslickgame;

import java.util.ArrayList;
import java.util.Random;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class LevelBackground {

	private Image backgroundImage;
	private float backgroundImageScale;
	 
	private PhysicsBox perspective;

	private Vec2 backgroundOffset = new Vec2(0,0);

	private FallingSpriteAnimator fallingSprites;
	
	final private float BACKGROUND_POSITION_FACTOR = 0.00f;
	final private float BACKGROUND_SIZE_FACTOR = 1.0f;
	
	public LevelBackground(PhysicsBox perspective) {
		this.perspective = perspective;			
	}
	
	private void setFallingSprite(VisualTheme theme){
		fallingSprites = new FallingSpriteAnimator(theme);	
	}
			
	public void update(){
		fallingSprites.update();
		
		float px = perspective.getPosition().x/SimpleSlickGame.screenWidth;
		float py = perspective.getPosition().y/SimpleSlickGame.screenHeight;
		
		backgroundOffset = new Vec2(px*-100,py*-100);
	}

	public void draw(Graphics g) {
		g.pushTransform();
		g.resetTransform();
		
		Vec2 offset = perspective.getPosition().mul(-BACKGROUND_POSITION_FACTOR);
		g.scale(BACKGROUND_SIZE_FACTOR,BACKGROUND_SIZE_FACTOR);
		g.translate(offset.x, offset.y);
		backgroundImage.draw(0, 0, SimpleSlickGame.screenWidth, SimpleSlickGame.screenHeight);
		
		g.popTransform();
		if(fallingSprites != null)
			fallingSprites.draw(g);
		/*Vec2 o = backgroundOffset;
		if(backgroundImage != null)
			backgroundImage.draw(o.x,o.y, SimpleSlickGame.scale);
			
		*/
	}

	public void setTheme(VisualTheme theme) {
		try {
			backgroundImage = new Image(theme.background);
			setFallingSprite(theme);
		} catch (SlickException e) {
			e.printStackTrace();
		}		
	}

	
	
	
}
