package simpleslickgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;



public class SmokeEffect implements Effect {
	List<Particle> sprites;
	float time;
	float duration;
	float speed;
	private static Image smokeImage = null;
	public SmokeEffect(Vec2 position)
	{
		sprites = new ArrayList<Particle>();
		if (smokeImage == null) {
			try {
				smokeImage = new Image("resource/images/smoke.png");
			} catch (SlickException e)
			{
				e.printStackTrace();
			}
		}
		time = 0.0f;
		duration = 0.5f;
		speed = 1000.0f;
		
		Random gen = new Random(System.currentTimeMillis());
		
		for (int i=0; i<1000; i++) {
			float nativeSpeed = (float)gen.nextInt((int)speed);
			Vec2 rv = new Vec2(gen.nextInt(101)-50,gen.nextInt(101)-50);
			rv.normalize();
			rv = rv.mul(nativeSpeed);
			
			Particle p = new Particle(position,rv,new Vec2(20,20),1.0f,smokeImage);
			p.nativeSpeed = nativeSpeed;
			p.position = p.position.add(p.velocity.mul(1.0f/10.0f));
			sprites.add(p);
		}
	}
	@Override
	public boolean isDone() {
		return (time > duration);
	}

	@Override
	public void update(int delta) {
		time += (float)delta/1000.0f;
		
		for (Particle particle : sprites) {
			particle.alpha = (1.0f-(time/duration));
			particle.velocity.normalize();
			particle.velocity = particle.velocity.mul(particle.nativeSpeed * (1.0f - (time/duration)));
			particle.update(delta);
		}
	}

	@Override
	public void drawMe(Graphics g) {
		for (Particle particle : sprites) {
			particle.draw(g);
		}
	}

}
