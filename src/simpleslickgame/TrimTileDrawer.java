package simpleslickgame;

import java.util.ArrayList;
import java.util.Random;

import org.jbox2d.common.Vec2;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class TrimTileDrawer {

	private ArrayList<Sprite> sprites;
	
	private Vec2 topLeft;
	private Vec2 bottomRight;
	private float tileHeight;
	private float tileWidth;
	
	public TrimTileDrawer(Vec2 topLeft, Vec2 bottomRight, Tiles tiles, boolean randomOffset) {
		this.topLeft = topLeft;
		this.bottomRight = bottomRight;
		
		sprites = new ArrayList<Sprite>();
		
		tileHeight = tiles.getTileHeight();
		tileWidth  = tiles.getTileWidth();
		
		float yOffset = 5;
		
		float spriteWidth = tiles.getTileWidth();
		float spriteHeight = tiles.getTileHeight();
		
		Random r = new Random();
		for(float i = topLeft.x; i < bottomRight.x + 10; i += spriteWidth){	
			if(randomOffset) 
				yOffset = r.nextInt(10);
			
			Image img = tiles.getRandomTile();
			Sprite s = new Sprite(i - yOffset, topLeft.y - yOffset);
			s.image = tiles.getRandomTile();
			s.offset = yOffset;
			sprites.add(s);

		}
		
		for(float i = topLeft.x; i < bottomRight.x + 10; i += spriteHeight){
			if(randomOffset) 
				yOffset = r.nextInt(10);
			
			Image img = tiles.getRandomTile();
			Sprite s = new Sprite(i-yOffset, (bottomRight.y - spriteHeight)  + yOffset);
			s.image = img;;
			s.offset = yOffset;
			sprites.add(s);
		}
	}

	
	public void draw(Graphics g) {
	
		for(int i=0;i<sprites.size();i++){
			Sprite c = sprites.get(i);
			
			float width = tileWidth;
			float height = tileHeight;
			
			if(c.x + tileWidth > (bottomRight.x + c.offset))
				width = (bottomRight.x - c.x) + c.offset;
			
			if(c.y + tileHeight > (bottomRight.y +  c.offset))
				height = (bottomRight.y - c.y) + c.offset;
			
			sprites.get(i).image.draw(c.x,c.y, width, height);
		}
	}

}
