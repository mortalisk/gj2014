package simpleslickgame;

import java.util.ArrayList;

public class SharedTheme {

	public static SharedTheme Global = new SharedTheme(Themes.Sober());
	
	private VisualTheme theme;
	private ArrayList<ThemeChangeObserver> observers;

	public SharedTheme(VisualTheme theme) {
		observers = new ArrayList<ThemeChangeObserver>();
		this.theme = theme;
	}
	
	public VisualTheme getTheme(){
		return theme;
	}
	
	public void setNewTheme(VisualTheme theme){
		this.theme = theme;
		for(ThemeChangeObserver o : observers)
			o.onThemeChanged(theme);
	}
	
	public void addThemeChangedObserver(ThemeChangeObserver o){
		observers.add(o);
	}
}
