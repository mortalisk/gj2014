package simpleslickgame;

import java.util.Random;

public class Coordinates {
	static Random gen = null;
	
	static private void initGen() {
		if (gen == null) gen = new Random(System.currentTimeMillis());
	}
	
	public String toString(){
		return "(" + x + ", " + y + ")";
	}
	
	public Coordinates() {
		this.x = 0;
		this.y = 0;
	}
	
	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 *  Fill x and y with random number between 0 and 10
	 * 
	 */
	public void fillWithRandom() {
		initGen();
		x = gen.nextInt() % 10;
		y = gen.nextInt() % 10;
	}
	
	public int x;
	public int y;
}
