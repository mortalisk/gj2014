package simpleslickgame;

public class Themes {

	public static VisualTheme Sober() {
		VisualTheme theme = new VisualTheme();
		theme.name = "sober";
		theme.falling = "resource/images/rain/rain.png";
		theme.fallingRotation = false;
		theme.fallingSpeedY = 20;
		
		theme.background = "resource/images/bg_sober/bg_sober.png";
		
		theme.setGroundTrimTiles("resource/images/brick/0.png" ,"resource/images/brick/1.png","resource/images/brick/2.png");
		theme.setGroundBackgroundTiles("resource/images/ground/0.png","resource/images/ground/1.png","resource/images/ground/2.png");
		
		theme.garbageCan = "resource/images/garbage_can_body.png";
		theme.garbageCanLid = "resource/images/garbage_can_lid.png";
		
		theme.burger = "resource/images/burger/burger.png";
		theme.elevator= "resource/images/elevator/sober_elevator.png";
		
		return theme;
	}
	
	public static VisualTheme Acid() {
		VisualTheme theme = new VisualTheme();
		theme.name = "acid";
		theme.falling = "resource/images/smiley/smiley.png";
		theme.fallingRotation = true;
		theme.fallingSpeedY = 1;
		
		theme.background = "resource/images/bg_acid/bg_acid.png";
		
		theme.setGroundTrimTiles("resource/images/brick_acid/0.png");
		theme.setGroundBackgroundTiles("resource/images/ground_acid/0.png","resource/images/ground_acid/1.png","resource/images/ground_acid/2.png");
		
		theme.garbageCan = "resource/images/garbage_acid/garbage_can_body.png";
		theme.garbageCanLid = "resource/images/garbage_acid/garbage_can_lid.png";
		
		theme.burger = "resource/images/burger/burger.png";
		theme.elevator= "resource/images/acid_elevator/elevator.png";
		
		return theme;
	}	
	
	public static VisualTheme Booze() {
		VisualTheme theme = new VisualTheme();
		theme.name = "booze";
		theme.falling = "resource/images/martinee/martinee.png";
		theme.fallingRotation = true;
		theme.fallingSpeedY = 2;
		
		theme.background = "resource/images/bg_booze/bg_booze.png";
		
		theme.setGroundTrimTiles("resource/images/brick_booze/0.png","resource/images/brick_booze/1.png","resource/images/brick_booze/2.png","resource/images/brick_booze/3.png");
		theme.setGroundBackgroundTiles("resource/images/ground_booze/0.png","resource/images/ground_booze/1.png","resource/images/ground_booze/2.png","resource/images/ground_booze/3.png");
		
		theme.garbageCan = "resource/images/garbage_booze/garbage_can_body.png";
		theme.garbageCanLid = "resource/images/garbage_booze/garbage_can_lid.png";
		
		theme.burger = "resource/images/burger/burger.png";
		theme.elevator= "resource/images/elevator/elevator.png";
		
		return theme;
	}		
}
