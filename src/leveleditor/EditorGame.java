package leveleditor;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.World;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.SoundStore;

import simpleslickgame.Box;
import simpleslickgame.Burger;
import simpleslickgame.CollisionTriggers;
import simpleslickgame.EditorLevel;
import simpleslickgame.Elevator;
import simpleslickgame.GameLevel;
import simpleslickgame.GarbageCan;
import simpleslickgame.Ground;
import simpleslickgame.JsonFileReader;
import simpleslickgame.Needles;
import simpleslickgame.Player;
import simpleslickgame.SharedTheme;
import simpleslickgame.Themes;
import simpleslickgame.UserInput;

public class EditorGame extends BasicGame
{
	static int screenHeight;
	static int screenWidth;
	static int scale;
	private int cameraX = 0;
	private int cameraY = 0;
	static public boolean isInEditor = false;
	private Vec2 cameraPosition;
	
	
	private CollisionTriggers collisionTrigger;
	static final float IMAGE_SCALE = 1f;
	World w;
	Body body;
	float time;
	private UserInput userInput = new UserInput();
	
    private GameLevel currentLevel;
	private boolean mousepressed = false;
	private Vec2 start;
	private Vec2 stop;
	private Type currentType = Type.GROUND;
	public EditorGame(String gamename)
	{
		super(gamename);
	}
	
	private void save(){
		JsonFileReader writer = new JsonFileReader();
		writer.writeJson("level3", currentLevel);
	}
	
	@Override
	public void init(GameContainer gc) throws SlickException {
		w = new World(new Vec2(0, 0));

		currentLevel = new EditorLevel(w);
		
		cameraPosition = new Vec2(0 , 0);
		
		currentLevel.start();
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		w.step(0.02f, 300, 300);
		SoundStore.get().poll(0);

		Input input = gc.getInput();
		//userInput.handleInput(input, currentLevel, delta);
		//hashDrug.update(w, time);
		
		time += 0.1f;
		if (time >= Math.PI * 2) {
			time = 0.0f;
		}
		//currentLevel.update(delta);
		
		cameraPosition.x = cameraX;
		cameraPosition.y = cameraY;

				
		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			Vec2 pos = new Vec2(input.getMouseX()+cameraX, input.getMouseY()+cameraY);
			boolean fixedSize = currentType == Type.PLAYER || currentType == Type.TRASH || currentType == Type.BURGER || currentType == Type.ELEVATOR || currentType == Type.NEEDLES;
			if(fixedSize){	
				
				if(currentType == Type.PLAYER)
					((EditorLevel)currentLevel).addObject(new Player(w, pos));
				else if (currentType == Type.TRASH)
					((EditorLevel)currentLevel).addObject(new GarbageCan(pos, w, SharedTheme.Global.getTheme()));
				else if (currentType == Type.ELEVATOR)
					((EditorLevel)currentLevel).addObject(new Elevator(pos, w, SharedTheme.Global.getTheme()));
				else if (currentType == Type.NEEDLES)
					((EditorLevel)currentLevel).addObject(new Needles(pos, w));
				else if (currentType == Type.BURGER){
					System.out.println("Burger");
					((EditorLevel)currentLevel).addObject(new Burger(pos,w));
				}
			}else if ( !mousepressed) {
				System.out.println("START!");
				mousepressed = true;
				this.start = pos;
			}
		} else if (mousepressed && !input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
				System.out.println("GROUND!");
				mousepressed = false;
				if (currentType == Type.GROUND){
					Ground g =new Ground(start.x, start.y, stop.x, stop.y, w);
					g.setTheme(Themes.Sober());
					((EditorLevel)currentLevel).addObject(g);
				}
				else if (currentType == Type.BOX)
					((EditorLevel)currentLevel).addObject(new Box(start.x, start.y, stop.x, stop.y, false, w));		
		} else if (mousepressed) {
			Vec2 stop = new Vec2(input.getMouseX()+cameraX, input.getMouseY()+cameraY);
			this.stop = stop;
		}else if(input.isMousePressed(Input.MOUSE_RIGHT_BUTTON)) {
			((EditorLevel)currentLevel).markObject(input.getMouseX(), input.getMouseY());
		} else if (input.isKeyPressed(Input.KEY_D)) {
			((EditorLevel)currentLevel).delete();
		}else if (input.isKeyPressed(Input.KEY_T)) {
			currentType = Type.TRASH;
		}else if (input.isKeyPressed(Input.KEY_P)) {
			currentType = Type.PLAYER;
		}else if (input.isKeyPressed(Input.KEY_G)) {
			currentType = Type.GROUND;
		}else if (input.isKeyPressed(Input.KEY_B)) {
			currentType = Type.BOX;
		}else if (input.isKeyPressed(Input.KEY_E)) {
			currentType = Type.BURGER;
		}else if (input.isKeyPressed(Input.KEY_L)) {
			currentType = Type.ELEVATOR;
		}else if (input.isKeyPressed(Input.KEY_N)) {
			currentType = Type.NEEDLES;
		}else if (input.isKeyPressed(Input.KEY_S)){
			save();
		}else if (input.isKeyPressed(Input.KEY_LEFT)){
			cameraX -= 30;
		}else if (input.isKeyPressed(Input.KEY_RIGHT)){
			cameraX += 30;
		}else if (input.isKeyPressed(Input.KEY_UP)){
			cameraY -= 30;
		}else if (input.isKeyPressed(Input.KEY_DOWN)){
			cameraY += 30;
		}
		
		//hashDrug.update(w, time);
		
		//backgroundAnimator.update();
		
		time += 0.1f;
		if (time >= Math.PI * 2) {
			time = 0.0f;
		}
		//currentLevel.update(delta);
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException
	{			
		g.scale(scale, scale);
		g.translate(-cameraPosition.x, -cameraPosition.y);
		
		currentLevel.drawMe(g);
	}

	public static void main(String[] args)
	{
		try
		{
			AppGameContainer appgc;
			appgc = new AppGameContainer(new EditorGame("Pentastic!"));
			screenHeight = appgc.getScreenHeight();
			screenWidth = appgc.getScreenWidth();
			isInEditor = true;
			scale = screenHeight / 500;
			
			appgc.setTargetFrameRate(60);
			appgc.setDisplayMode(screenWidth, screenHeight, false);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(EditorGame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
