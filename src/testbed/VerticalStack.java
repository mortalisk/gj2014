package testbed;

import javax.swing.JFrame;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Rot;
import org.jbox2d.common.Transform;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.joints.FrictionJointDef;
import org.jbox2d.testbed.framework.TestList;
import org.jbox2d.testbed.framework.TestbedController.UpdateBehavior;
import org.jbox2d.testbed.framework.TestbedFrame;
import org.jbox2d.testbed.framework.TestbedModel;
import org.jbox2d.testbed.framework.TestbedPanel;
import org.jbox2d.testbed.framework.TestbedSetting;
import org.jbox2d.testbed.framework.TestbedSetting.SettingType;
import org.jbox2d.testbed.framework.TestbedSettings;
import org.jbox2d.testbed.framework.TestbedTest;
import org.jbox2d.testbed.framework.j2d.TestPanelJ2D;

/**
 * @author Daniel Murphy
 */
public class VerticalStack extends TestbedTest {
  private static final long BULLET_TAG = 1;

  public static final int e_columnCount = 5;
  public static final int e_rowCount = 15;

  Body m_bullet;

  @Override
  public Long getTag(Body argBody) {
    if (argBody == m_bullet) {
      return BULLET_TAG;
    }
    return super.getTag(argBody);
  }

  @Override
  public void processBody(Body argBody, Long argTag) {
    if (argTag == BULLET_TAG) {
      m_bullet = argBody;
      return;
    }
    super.processBody(argBody, argTag);
  }

  @Override
  public boolean isSaveLoadEnabled() {
    return true;
  }

  @Override
  public void initTest(boolean deserialized) {
    if (deserialized) {
      return;
    }

    {
      BodyDef bd = new BodyDef();
      Body ground = getWorld().createBody(bd);

      EdgeShape shape = new EdgeShape();
      // shape.setAsBox(40, 10, new Vec2(0,-10), 0);
      shape.set(new Vec2(-40.0f, 0.0f), new Vec2(40.0f, 0.0f));
      ground.createFixture(shape, 0.0f);

      shape.set(new Vec2(20.0f, 0.0f), new Vec2(20.0f, 20.0f));
      ground.createFixture(shape, 0.0f);
    }

    float xs[] = new float[] { 0.0f, -10.0f, -5.0f, 5.0f, 10.0f };

    for (int j = 0; j < e_columnCount; ++j) {
      PolygonShape shape = new PolygonShape();
      shape.setAsBox(0.5f, 0.5f);

      FixtureDef fd = new FixtureDef();
      fd.shape = shape;
      fd.density = 1.0f;
      fd.friction = 0.3f;

      for (int i = 0; i < e_rowCount; ++i) {
        BodyDef bd = new BodyDef();
        bd.type = BodyType.DYNAMIC;

        int n = j * e_rowCount + i;
        assert (n < e_rowCount * e_columnCount);

        float x = 0.0f;
        // float x = RandomFloat(-0.02f, 0.02f);
        // float x = i % 2 == 0 ? -0.025f : 0.025f;
        bd.position.set(xs[j] + x, 0.752f + 1.54f * i);
        Body body = getWorld().createBody(bd);

        body.createFixture(fd);
      }
    }

    m_bullet = null;
  }

  @Override
  public void keyPressed(char argKeyChar, int argKeyCode) {
    switch (argKeyChar) {
      case ',':
        if (m_bullet != null) {
          getWorld().destroyBody(m_bullet);
          m_bullet = null;
        }

        {
          CircleShape shape = new CircleShape();
          shape.m_radius = 0.25f;

          FixtureDef fd = new FixtureDef();
          fd.shape = shape;
          fd.density = 20.0f;
          fd.restitution = 0.05f;

          BodyDef bd = new BodyDef();
          bd.type = BodyType.DYNAMIC;
          bd.bullet = true;
          bd.position.set(-31.0f, 5.0f);

          m_bullet = getWorld().createBody(bd);
          m_bullet.createFixture(fd);

          m_bullet.setLinearVelocity(new Vec2(400.0f, 0.0f));
        }
        break;
    }
  }

  @Override
  public void step(TestbedSettings settings) {
    super.step(settings);
    addTextLine("Press ',' to launch bullet.");
  }

  @Override
  public String getTestName() {
    return "Vertical Stack";
  }
  

  
  public static void main(String[] args) {
	  TestbedModel model = new TestbedModel();         // create our model

	// add tests
	TestList.populateModel(model);                   // populate the provided testbed tests
	model.addCategory("My Super Tests");             // add a category
	model.addTest(new VerticalStack());                // add our test
	model.addTest(new MJWTest2());
	model.addTest(new ApplyForce());

	// add our custom setting "My Range Setting", with a default value of 10, between 0 and 20
	model.getSettings().addSetting(new TestbedSetting("My Range Setting", SettingType.ENGINE, 10, 0, 20));

	TestbedPanel panel = new TestPanelJ2D(model);    // create our testbed panel

	JFrame testbed = new TestbedFrame(model, panel, UpdateBehavior.UPDATE_CALLED); // put both into our testbed frame
	// etc
	testbed.setVisible(true);
	testbed.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
}
}

class ApplyForce extends TestbedTest {
	  private static final long BODY_TAG = 12;

	  Body m_body;

	  @Override
	  public void initTest(boolean deserialized) {
	    if (deserialized) {
	      return;
	    }

	    getWorld().setGravity(new Vec2(0.0f, 0.0f));

	    final float k_restitution = 0.4f;

	    Body ground;
	    {
	      BodyDef bd = new BodyDef();
	      bd.position.set(0.0f, 20.0f);
	      ground = getWorld().createBody(bd);

	      EdgeShape shape = new EdgeShape();

	      FixtureDef sd = new FixtureDef();
	      sd.shape = shape;
	      sd.density = 0.0f;
	      sd.restitution = k_restitution;

	      // Left vertical
	      shape.set(new Vec2(-20.0f, -20.0f), new Vec2(-20.0f, 20.0f));
	      ground.createFixture(sd);

	      // Right vertical
	      shape.set(new Vec2(20.0f, -20.0f), new Vec2(20.0f, 20.0f));
	      ground.createFixture(sd);

	      // Top horizontal
	      shape.set(new Vec2(-20.0f, 20.0f), new Vec2(20.0f, 20.0f));
	      ground.createFixture(sd);

	      // Bottom horizontal
	      shape.set(new Vec2(-20.0f, -20.0f), new Vec2(20.0f, -20.0f));
	      ground.createFixture(sd);
	    }

	    {
	      Transform xf1 = new Transform();
	      xf1.q.set(0.3524f * MathUtils.PI);
	      Rot.mulToOutUnsafe(xf1.q, new Vec2(1.0f, 0.0f), xf1.p);

	      Vec2 vertices[] = new Vec2[3];
	      vertices[0] = Transform.mul(xf1, new Vec2(-1.0f, 0.0f));
	      vertices[1] = Transform.mul(xf1, new Vec2(1.0f, 0.0f));
	      vertices[2] = Transform.mul(xf1, new Vec2(0.0f, 0.5f));

	      PolygonShape poly1 = new PolygonShape();
	      poly1.set(vertices, 3);

	      FixtureDef sd1 = new FixtureDef();
	      sd1.shape = poly1;
	      sd1.density = 4.0f;

	      Transform xf2 = new Transform();
	      xf2.q.set(-0.3524f * MathUtils.PI);
	      Rot.mulToOut(xf2.q, new Vec2(-1.0f, 0.0f), xf2.p);

	      vertices[0] = Transform.mul(xf2, new Vec2(-1.0f, 0.0f));
	      vertices[1] = Transform.mul(xf2, new Vec2(1.0f, 0.0f));
	      vertices[2] = Transform.mul(xf2, new Vec2(0.0f, 0.5f));

	      PolygonShape poly2 = new PolygonShape();
	      poly2.set(vertices, 3);

	      FixtureDef sd2 = new FixtureDef();
	      sd2.shape = poly2;
	      sd2.density = 2.0f;

	      BodyDef bd = new BodyDef();
	      bd.type = BodyType.DYNAMIC;
	      bd.angularDamping = 5.0f;
	      bd.linearDamping = 0.1f;

	      bd.position.set(0.0f, 2.0f);
	      bd.angle = MathUtils.PI;
	      bd.allowSleep = false;
	      m_body = getWorld().createBody(bd);
	      m_body.createFixture(sd1);
	      m_body.createFixture(sd2);
	    }

	    {
	      PolygonShape shape = new PolygonShape();
	      shape.setAsBox(0.5f, 0.5f);

	      FixtureDef fd = new FixtureDef();
	      fd.shape = shape;
	      fd.density = 1.0f;
	      fd.friction = 0.3f;

	      for (int i = 0; i < 10; ++i) {
	        BodyDef bd = new BodyDef();
	        bd.type = BodyType.DYNAMIC;

	        bd.position.set(0.0f, 5.0f + 1.54f * i);
	        Body body = getWorld().createBody(bd);

	        body.createFixture(fd);

	        float gravity = 10.0f;
	        float I = body.getInertia();
	        float mass = body.getMass();

	        // For a circle: I = 0.5 * m * r * r ==> r = sqrt(2 * I / m)
	        float radius = MathUtils.sqrt(2.0f * I / mass);

	        FrictionJointDef jd = new FrictionJointDef();
	        jd.localAnchorA.setZero();
	        jd.localAnchorB.setZero();
	        jd.bodyA = ground;
	        jd.bodyB = body;
	        jd.collideConnected = true;
	        jd.maxForce = mass * gravity;
	        jd.maxTorque = mass * radius * gravity;

	        getWorld().createJoint(jd);
	      }
	    }
	  }

	  @Override
	  public void keyPressed(char keyCar, int keyCode) {
	    // TODO Auto-generated method stub
	    super.keyPressed(keyCar, keyCode);
	  }
	  
	  @Override
	  public void step(TestbedSettings settings) {
	    super.step(settings);

	    addTextLine("Use 'wasd' to move, 'e' and 's' drift.");
	    if (getModel().getKeys()['w']) {
	      Vec2 f = m_body.getWorldVector(new Vec2(0.0f, -30.0f));
	      Vec2 p = m_body.getWorldPoint(m_body.getLocalCenter().add(new Vec2(0.0f, 2.0f)));
	      m_body.applyForce(f, p);
	    } else if (getModel().getKeys()['q']) {
	      Vec2 f = m_body.getWorldVector(new Vec2(0.0f, -30.0f));
	      Vec2 p = m_body.getWorldPoint(m_body.getLocalCenter().add(new Vec2(-.2f, 0f)));
	      m_body.applyForce(f, p);
	    } else if (getModel().getKeys()['e']) {
	      Vec2 f = m_body.getWorldVector(new Vec2(0.0f, -30.0f));
	      Vec2 p = m_body.getWorldPoint(m_body.getLocalCenter().add(new Vec2(.2f, 0f)));
	      m_body.applyForce(f, p);
	    } else if (getModel().getKeys()['s']) {
	      Vec2 f = m_body.getWorldVector(new Vec2(0.0f, 30.0f));
	      Vec2 p = m_body.getWorldCenter();
	      m_body.applyForce(f, p);
	    }

	    if (getModel().getKeys()['a']) {
	      m_body.applyTorque(20.0f);
	    }

	    if (getModel().getKeys()['d']) {
	      m_body.applyTorque(-20.0f);
	    }
	  }

	  @Override
	  public boolean isSaveLoadEnabled() {
	    return true;
	  }

	  @Override
	  public Long getTag(Body body) {
	    if (body == m_body) {
	      return BODY_TAG;
	    }
	    return super.getTag(body);
	  }

	  @Override
	  public void processBody(Body body, Long tag) {
	    if (tag == BODY_TAG) {
	      m_body = body;
	    }
	    super.processBody(body, tag);
	  }

	  @Override
	  public String getTestName() {
	    return "Apply Force";
	  }
	}

class MJWTest2 extends TestbedTest {

	  @Override
	  public void initTest(boolean argDeserialized) {
	    setTitle("Couple of Things Test");

	    getWorld().setGravity(new Vec2());

	    for (int i = 0; i < 2; i++) {
	      PolygonShape polygonShape = new PolygonShape();
	      polygonShape.setAsBox(1, 1);
	      
	      BodyDef bodyDef = new BodyDef();
	      bodyDef.type = BodyType.DYNAMIC;
	      bodyDef.position.set(5 * i, 0);
	      bodyDef.angle = (float) (Math.PI / 4 * i);
	      bodyDef.allowSleep = false;
	      Body body = getWorld().createBody(bodyDef);
	      body.createFixture(polygonShape, 5.0f);

	      body.applyForce(new Vec2(-10000 * (i - 1), 0), new Vec2());
	    }
	  }

	  @Override
	  public String getTestName() {
	    return "Couple of Things";
	  }
	}